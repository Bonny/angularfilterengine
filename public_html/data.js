
var RULE_SUBJECTS_IDS = [0,1,3,4,5,7,8,10];

var RULE_SUBJECTS_NAMES = [];
RULE_SUBJECTS_NAMES[0] = "SUBJECT";
RULE_SUBJECTS_NAMES[1] = "SENDER";
RULE_SUBJECTS_NAMES[3] = "BODY";
RULE_SUBJECTS_NAMES[4] = "TO";
RULE_SUBJECTS_NAMES[5] = "CC";
RULE_SUBJECTS_NAMES[7] = "RECEIPT_PEC";
RULE_SUBJECTS_NAMES[8] = "MESSAGE_TYPE";
RULE_SUBJECTS_NAMES[10] = "ATTACHMENT";

var RULE_TYPES = [];
RULE_TYPES[0] = "TEXT";
RULE_TYPES[1] = "TEXT";
RULE_TYPES[3] = "TEXT";
RULE_TYPES[4] = "TEXT";
RULE_TYPES[5] = "TEXT";
RULE_TYPES[7] = "COMBOBOX";
RULE_TYPES[8] = "COMBOBOX";
RULE_TYPES[10] = "TEXT";

var RULE_OPERATORS = [];
RULE_OPERATORS[0] = [0,1,2,3,4,5,12];
RULE_OPERATORS[1] = [0,1,2,3,4,5,12];
RULE_OPERATORS[3] = [0,1,2,3,12];
RULE_OPERATORS[4] = [0,1,2,3,4,5,12];
RULE_OPERATORS[5] = [0,1,2,3,4,5,12];
RULE_OPERATORS[7] = [2,3];
RULE_OPERATORS[8] = [2,3];
RULE_OPERATORS[10] = [0,1,12];

var RULE_OPERATORS_NAMES = [];
RULE_OPERATORS_NAMES[0] = "CONTAINS";
RULE_OPERATORS_NAMES[1] = "NOT_CONTAINS";
RULE_OPERATORS_NAMES[2] = "EQUALS";
RULE_OPERATORS_NAMES[3] = "NOT_EQUALS";
RULE_OPERATORS_NAMES[4] = "START_WITH";
RULE_OPERATORS_NAMES[5] = "END_WITH";
RULE_OPERATORS_NAMES[6] = "BEFORE";
RULE_OPERATORS_NAMES[7] = "AFTER";
RULE_OPERATORS_NAMES[8] = "IS_EMPTY";
RULE_OPERATORS_NAMES[9] = "IS_NOT_EMPTY";
RULE_OPERATORS_NAMES[10] = "IS_GREATER";
RULE_OPERATORS_NAMES[11] = "IS_LESS";
RULE_OPERATORS_NAMES[12] = "REG_EXP";

var RULE_VALUES = [];
RULE_VALUES[0] = "";
RULE_VALUES[1] = "";
RULE_VALUES[3] = "";
RULE_VALUES[4] = "";
RULE_VALUES[5] = "";
RULE_VALUES[7] = ["0","1","2","3"];
RULE_VALUES[8] = ["0","1"];
RULE_VALUES[10] = "";

var RULE_VALUES_NAMES = [];
RULE_VALUES_NAMES[7] = [];
RULE_VALUES_NAMES[7]["0"] = "ACCEPTANCE";
RULE_VALUES_NAMES[7]["1"] = "NOT_ACCEPTANCE";
RULE_VALUES_NAMES[7]["2"] = "DELIVERY";
RULE_VALUES_NAMES[7]["3"] = "NOT_DELIVERY";
RULE_VALUES_NAMES[8] = [];
RULE_VALUES_NAMES[8]["0"] = "PEO";
RULE_VALUES_NAMES[8]["1"] = "PEC";

var FILTER_TEST = {
   "dataIns": "26/10/2016 12:38",
   "mailboxes": [1],
   "filterAction": "-1",
   "rules": [{
         "subject": 1,
         "weight": 0,
         "value": "postecom",
         "operator": 0
      }],
   "idFilter": 2,
   "type": 0,
   "priority": 0,
   "labels": [{
         "modGroups": false,
         "color": "F2FF3B",
         "dateIns": "2016-10-26 12:37:56.557",
         "modFilters": false,
         "groups": [],
         "filters": [2],
         "users": [2, 4],
         "descr": "",
         "dateMod": "2016-10-26 12:44:46.637",
         "idLabel": 2,
         "name": "PTC",
         "modUsers": false,
         "supervisors": []
      }],
   "condition": 0,
   "name": "HD PosteCom",
   "apply_after_sent": 0,
   "dataMod": "",
   "status": 1,
   "apply_download": 1
};

var RULE_CONFIG = '{"rules":[{"operators":[{"display":"contiene","name":"CONTAINS","id":0},{"display":"non contiene","name":"NOT_CONTAINS","id":1},{"display":"uguale a","name":"EQUALS","id":2},{"display":"non è uguale a ","name":"NOT_EQUALS","id":3},{"display":"inizia con","name":"START_WITH","id":4},{"display":"finisce con","name":"END_WITH","id":5},{"display":"espressione regolare","name":"REG_EXP","id":12}],"display":"Oggetto","name":"SUBJECT","id":0,"type":"TEXT"},{"operators":[{"display":"contiene","name":"CONTAINS","id":0},{"display":"non contiene","name":"NOT_CONTAINS","id":1},{"display":"uguale a","name":"EQUALS","id":2},{"display":"non è uguale a ","name":"NOT_EQUALS","id":3},{"display":"inizia con","name":"START_WITH","id":4},{"display":"finisce con","name":"END_WITH","id":5},{"display":"espressione regolare","name":"REG_EXP","id":12}],"display":"Mittente","name":"SENDER","id":1,"type":"TEXT"},{"operators":[{"display":"contiene","name":"CONTAINS","id":0},{"display":"non contiene","name":"NOT_CONTAINS","id":1},{"display":"uguale a","name":"EQUALS","id":2},{"display":"non è uguale a ","name":"NOT_EQUALS","id":3},{"display":"espressione regolare","name":"REG_EXP","id":12}],"display":"Corpo","name":"BODY","id":3,"type":"TEXT"},{"operators":[{"display":"contiene","name":"CONTAINS","id":0},{"display":"non contiene","name":"NOT_CONTAINS","id":1},{"display":"uguale a","name":"EQUALS","id":2},{"display":"non è uguale a ","name":"NOT_EQUALS","id":3},{"display":"inizia con","name":"START_WITH","id":4},{"display":"finisce con","name":"END_WITH","id":5},{"display":"espressione regolare","name":"REG_EXP","id":12}],"display":"A","name":"TO","id":4,"type":"TEXT"},{"operators":[{"display":"contiene","name":"CONTAINS","id":0},{"display":"non contiene","name":"NOT_CONTAINS","id":1},{"display":"uguale a","name":"EQUALS","id":2},{"display":"non è uguale a ","name":"NOT_EQUALS","id":3},{"display":"inizia con","name":"START_WITH","id":4},{"display":"finisce con","name":"END_WITH","id":5},{"display":"espressione regolare","name":"REG_EXP","id":12}],"display":"CC","name":"CC","id":5,"type":"TEXT"},{"operators":[{"display":"uguale a","name":"EQUALS","id":2},{"display":"non è uguale a ","name":"NOT_EQUALS","id":3}],"display":"Ricevuta PEC","values":[{"display":"Accettazione","value":"ACCEPTANCE","key":"0"},{"display":"Mancata accettazione","value":"NOT_ACCEPTANCE","key":"1"},{"display":"Consegna","value":"DELIVERY","key":"2"},{"display":"Mancata consegna","value":"NOT_DELIVERY","key":"3"}],"name":"RECEIPT_PEC","id":7,"type":"COMBOBOX"},{"operators":[{"display":"uguale a","name":"EQUALS","id":2},{"display":"non è uguale a ","name":"NOT_EQUALS","id":3}],"display":"Tipo messaggio","values":[{"display":"Posta ordinaria","value":"PEO","key":"0"},{"display":"Posta certificata","value":"PEC","key":"1"}],"name":"MESSAGE_TYPE","id":8,"type":"COMBOBOX"},{"operators":[{"display":"contiene","name":"CONTAINS","id":0},{"display":"non contiene","name":"NOT_CONTAINS","id":1},{"display":"espressione regolare","name":"REG_EXP","id":12}],"display":"Allegati","name":"ATTACHMENT","id":10,"type":"TEXT"}]}';