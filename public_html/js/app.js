/* global angular, dictionary, RULE_SUBJECTS_IDS, RULE_SUBJECTS_NAMES, RULE_OPERATORS, RULE_OPERATORS_NAMES, RULE_TYPES, RULE_VALUES, RULE_VALUES_NAMES */

dictionary["label.field.reqiured"] = "Campo richiesto";
dictionary["error.field.priority.max"] = "Richiesto <";
dictionary["label.field.not.valid"] = "Campo non valido";

var app = angular.module('filter-app', []);

app.constant("FilterMaxPriority", 100);

app.directive('filter', ["$log", "FilterMaxPriority", function ($log, FilterMaxPriority) {
      return {
         restrict: 'E',
         templateUrl: "filter.html",
         scope: {
            fdata: '='
         },
         controller: function ($scope, $element, $attrs) {

            $scope.dictionary = dictionary;
            $scope.debug = false;

            $scope.getMaxPriority = function () {
               return FilterMaxPriority;
            };

            var me = this;

            var FilterModel = {
               idFilter: -1,
               name: "",
               condition: 0,
               status: 0,
               apply_download: 0,
               apply_after_sent: 0,
               dataIns: "",
               dataMod: "",
               type: 0,
               filterAction: "",
               priority: 0,
               reqManVer: 0,
               mailboxes: [],
               rules: [],
               labels: []
            };

            var Rulemodel = {
               subject: 0,
               operator: 0,
               value: "",
               weight: 0
            };

            if ($scope.data)
               $scope.filter = angular.copy($scope.fdata);
            else {
               $scope.filter = angular.copy(FilterModel);
            }

            //$scope.filter = angular.copy(FILTER_TEST);

            $scope.enableRules = function (value) {
               // $log.info("enableRules() = " + value);
               angular.forEach($scope.filter.rules, function ($r) {
                  $r.disabled = !value;
               });
            };

            $scope.enableWeights = function (value) {
               // $log.info("enableWeights() = " + value);
               angular.forEach($scope.filter.rules, function ($r) {
                  $r.disabledWeight = !value;
               });
            };

            me.conditionChange = function (inputValue) {
               $log.debug("conditionChange = " + inputValue);
               switch (parseInt(inputValue)) {
                  case 0:
                     $scope.enableRules(true);
                     $scope.enableWeights(false);
                     break;
                  case 1:
                     $scope.enableRules(true);
                     $scope.enableWeights(false);
                     break;
                  case 2:
                     $scope.enableRules(false);
                     $scope.enableWeights(false);
                     break;
                  case 3:
                     $scope.enableRules(true);
                     $scope.enableWeights(true);
                     break;
                  default:
                     $log.error("condition not valid");
                     break;
               }
            };

            me.addRule = function (index) {
               $log.info("addRule(" + index + ")");
               $scope.filter.rules.splice(index, 0, angular.copy(Rulemodel));
               me.conditionChange($scope.filter.condition);
            };

            me.delRule = function (index) {
               $log.info("delRule(" + index + ")");
               $scope.filter.rules.splice(index, 1);
            };

            (function () {
               me.conditionChange($scope.filter.condition);
               if ($scope.filter.idFilter > 0) {
                  $scope.title = $scope.dictionary['label.modify'] + " " + $scope.dictionary['label.filter'] + ": " + $scope.filter.name;
               } else {
                  $scope.title = $scope.dictionary['label.new'] + " " + $scope.dictionary['label.filter'];
               }
               if ($scope.filter.rules.length === 0) {
                  me.addRule(0);
               }
            })();
         }

      };
   }
]);

app.directive('condition', ["$log", "$timeout", function ($log, $timeout) {
      return {
         restrict: 'E',
         require: '^filter',
         template: '<label class="radio-inline" ng-repeat="c in conditions">'
                 + '   <input type="radio" name="condition" ng-model="filter.condition" ng-value="c.inputValue" ng-click="c.handler()"> {{c.boxLabel}}'
                 + '</label>',
         link: function (scope, element, attrs, filterCtrl) {

            function conditionChange() {
               filterCtrl.conditionChange(this.inputValue);
            }

            scope.conditions = [{
                  boxLabel: dictionary["label.andCondition"],
                  inputValue: 0,
                  handler: conditionChange
               }, {
                  boxLabel: dictionary["label.orCondition"],
                  inputValue: 1,
                  handler: conditionChange
               }, {
                  boxLabel: dictionary["label.allCondition"],
                  inputValue: 2,
                  handler: conditionChange
               }, {
                  boxLabel: dictionary["label.useWeight"],
                  inputValue: 3,
                  handler: conditionChange
               }];

         }
      };
   }
]);

app.directive('rule', ["$log", "$timeout", function ($log, $timeout) {
      return {
         restrict: 'E',
         require: '^filter',
         templateUrl: "rule.html",
         scope: {
            rdata: "=",
            canDelete: "=",
            idx: "="
         },
         link: function (scope, element, attrs, filterCtrl) {

            $log.info("rdata = {");
            $log.info(scope.rdata);
            $log.info("}");
            $log.info("canDelete=" + scope.canDelete);
            $log.info("idx=" + scope.idx);
            
            scope.rule_cmp_req = false;

            scope.getSubjects = function () {
               return RULE_SUBJECTS_IDS;
            };

            scope.getSubjectName = function (s) {
               return dictionary["label.wsp." + RULE_SUBJECTS_NAMES[parseInt(s)]];
            };

            scope.getOperators = function () {
               return RULE_OPERATORS[parseInt(scope.rdata.subject)];
            };

            scope.getOperatorName = function (o) {
               return dictionary["label.wsp." + RULE_OPERATORS_NAMES[parseInt(o)]];
            };

            scope.getValues = function () {
               return RULE_VALUES[parseInt(scope.rdata.subject)];
            };

            scope.getValueName = function (v) {
               return dictionary["label.wsp." + (RULE_VALUES_NAMES[parseInt(scope.rdata.subject)][v])];
            };

            scope.ruleChange = function () {

            };

            scope.isCombobox = function () {
               return RULE_TYPES[parseInt(scope.rdata.subject)] === "COMBOBOX";
            };

            scope.add = function () {
               filterCtrl.addRule(parseInt(scope.idx) + 1);
            };

            scope.del = function () {
               filterCtrl.delRule(scope.idx);
            };


            $timeout(function () {

               $log.info("init(" + scope.idx + ")");

               jQuery('select#subject-' + scope.idx + ' option[value="' + scope.rdata.subject + '"]').attr("selected", "selected");
               jQuery('select#operator-' + scope.idx + ' option[value="' + scope.rdata.operator + '"]').attr("selected", "selected");

               if (scope.isCombobox()) {
                  jQuery('select#value-' + scope.idx + ' option[value="' + scope.rdata.value + '"]').attr("selected", "selected");
               }

            }, 200);

         }
      };
   }
]);